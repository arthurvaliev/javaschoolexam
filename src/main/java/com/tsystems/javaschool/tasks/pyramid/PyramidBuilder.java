package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
     static int[][] buildPyramid(List<Integer> inputNumbers) {

        if ((inputNumbers.contains(null)) || (inputNumbers.size() == 0) ){
            throw new CannotBuildPyramidException();
        }

        try{
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError e){
            throw new CannotBuildPyramidException();
        }

        ArrayList<ArrayList<Integer>> temp_pyramid = new ArrayList<>();

        int begin = 0;
        int element_count = 1;

        while (true){
            try{
                ArrayList<Integer> line = new ArrayList<>(inputNumbers.subList(begin, begin + element_count));
                temp_pyramid.add(line);
                begin = begin + element_count;
                element_count += 1;

            } catch (IndexOutOfBoundsException e) {

                break;
            }
        }



        ArrayList<ArrayList<Integer>> temp_pyramid_with_zeros = new ArrayList<>();

        for (ArrayList<Integer> line: temp_pyramid) {

            int current_pos = 1;

            while (current_pos < line.size()){
                line.add(current_pos, 0);
                current_pos += 2;
            }
            temp_pyramid_with_zeros.add(line);
        }

        Collections.reverse(temp_pyramid_with_zeros);
        int required_size = temp_pyramid_with_zeros.get(0).size();

        ArrayList<ArrayList<Integer>> pyramid_arraylist = new ArrayList<>();

        for (ArrayList<Integer> line: temp_pyramid_with_zeros) {
            while (line.size() < required_size) {
                line.add(line.size(), 0);
                line.add(0, 0);
            }
            pyramid_arraylist.add(line);

        }

        check_if_all_elements_were_added(inputNumbers, pyramid_arraylist);

        Collections.reverse(pyramid_arraylist);

        int[][] return_value = new int[pyramid_arraylist.size()][required_size];

        for (ArrayList<Integer> line: pyramid_arraylist) {
            int[] int_line = convertIntegers(line);
            int curr_index = pyramid_arraylist.indexOf(line);
            return_value[curr_index] = int_line;
        }

        return return_value;
    }

    static void check_if_all_elements_were_added(List<Integer> inputNumbers, ArrayList<ArrayList<Integer>> pyramid_arraylist) {

        List<Integer> pyramid_elements = new ArrayList<>();

        for (ArrayList<Integer> line: pyramid_arraylist) {
            for (Integer element: line) {
                if (element != 0){
                    pyramid_elements.add(element);
                }

            }
        }
        if (inputNumbers.contains(0)){
            pyramid_elements.add(0);
        }
        Collections.sort(pyramid_elements);

        for (int i = 0; i < inputNumbers.size(); i++){
            try{
                if (!inputNumbers.get(i).equals(pyramid_elements.get(i))){
                    throw new CannotBuildPyramidException();
                }
            } catch (IndexOutOfBoundsException e){
                throw new CannotBuildPyramidException();
            }
        }
    }

    static int[] convertIntegers(List<Integer> integers) {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next();
        }
        return ret;
    }
  }
