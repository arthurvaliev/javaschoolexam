package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;
import java.util.stream.Stream;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
     @SuppressWarnings("rawtypes")
    static boolean find(List x, List y) {

        if ((x == null) || (y == null) ){
            throw new IllegalArgumentException();
        }

        int current_pos = 0;

        List y_modified = new ArrayList();

        for (Object num: y) {
            try{
                if (num == x.get(current_pos)){
                    y_modified.add(num);
                    current_pos += 1;
                }
            } catch (IndexOutOfBoundsException e){
                //e.printStackTrace();
            }

        }

        return x.equals(y_modified);
    }
}
