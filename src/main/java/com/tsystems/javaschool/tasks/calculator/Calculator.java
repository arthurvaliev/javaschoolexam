package com.tsystems.javaschool.tasks.calculator;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
     static String evaluate(String statement) {

        try{
            String[] expression_with_delim = statement.split("(?<=[-+*/(),])|(?=[-+*/(),])");
            ArrayList<String> commandArrayList = new ArrayList<>(Arrays.asList(expression_with_delim));
            if ((commandArrayList.contains(",")) || (commandArrayList.contains(" ")) ){
                return null;
            }

            HashMap<Integer, Integer> brackets = create_bracket_map(commandArrayList);

            if (brackets.containsKey(5000)){
                return null;
            }


            for (Map.Entry<Integer,Integer> entry : brackets.entrySet()) {
                Integer key = entry.getKey();
                Integer value = entry.getValue();

                if (value == null) {
                    return null;
                } else if (key > value) {
                    return null;
                }
            }

            while (brackets.size() > 0) {
                String result = execute_computation(brackets, commandArrayList);

                if (result == null){
                    return result;
                }

                Set<Integer> keys = brackets.keySet();
                Integer[] keyset_array = brackets.keySet().toArray(new Integer[keys.size()]);
                Integer latest_key = keyset_array[keyset_array.length - 1];
                commandArrayList.subList(latest_key, brackets.get(latest_key) + 1).clear();
                commandArrayList.add(latest_key, result);
                brackets = create_bracket_map(commandArrayList);
                if (brackets.containsKey(5000)){
                    return null;
                }
            }

            String result = execute_computation(brackets, commandArrayList);

            if (result == null){
                return result;
            } else {
                if ((Double.valueOf(result) == Math.floor(Double.valueOf(result)))){
                    return String.valueOf(Double.valueOf(result).intValue());
                } else{
                    double double_result = Double.parseDouble(result);
                    return String.valueOf(round(double_result, 4));
                }
            }
        } catch (Exception e){
            return null;
        }
    }

    static HashMap<Integer, Integer> create_bracket_map(ArrayList<String> commandArrayList) {

        HashMap<Integer, Integer> brackets = new LinkedHashMap<>();
        boolean success_flag = true;

        Integer current_pos = 0;
        int decrement = 1;

        for (String symbol: commandArrayList) {
            if (symbol.equals("(")){
                brackets.put(current_pos, null);
            }
            if (symbol.equals(")")){
                Set<Integer> keys = brackets.keySet();
                Integer[] keyset_array = brackets.keySet().toArray(new Integer[keys.size()]);

                if (keyset_array.length != 0) {
                    Integer latest_key = keyset_array[keyset_array.length - decrement];

                    if (brackets.get(latest_key) == null) {
                        brackets.put(latest_key, current_pos);
                    } else {
                        while (brackets.get(latest_key) != null) {
                            try{
                                latest_key = keyset_array[keyset_array.length - decrement];
                                decrement += 1;
                            } catch (ArrayIndexOutOfBoundsException e) {
                                success_flag = false;
                            }

                        }
                        brackets.put(latest_key, current_pos);
                        decrement = 1;
                    }
                }
            }
            current_pos += 1;
        }

        if (!success_flag) {
            brackets = new LinkedHashMap<>();
            brackets.put(5000, 5000);
        }

        return brackets;

    }

    static String execute_computation(HashMap<Integer, Integer> brackets, ArrayList<String> commandArrayList){

        Set<Integer> keys = brackets.keySet();
        Integer[] keyset_array = brackets.keySet().toArray(new Integer[keys.size()]);

        if (keyset_array.length != 0) {
            Integer latest_key = keyset_array[keyset_array.length - 1];

            ArrayList<String> statement_in_brackets = new ArrayList<>(commandArrayList.subList(latest_key + 1,
                    brackets.get(latest_key)));

            return execute_with_priority(statement_in_brackets);
        } else {
            return execute_with_priority(commandArrayList);
        }
    }

    static String execute_with_priority(ArrayList<String> statement_in_brackets) {

        boolean higher_priority_operations = (statement_in_brackets.contains("*")) || (statement_in_brackets.contains("/"));

        while (higher_priority_operations){

            ArrayList<String> symbols_to_find = new ArrayList<>();
            symbols_to_find.add("*");
            symbols_to_find.add("/");

            String operation = find_operation_symbol(statement_in_brackets, symbols_to_find);

            if (!operation.equals("")){
                int operand_pos = statement_in_brackets.indexOf(operation);
                ArrayList<String> simple_operation = new ArrayList<>(statement_in_brackets.subList(operand_pos - 1, operand_pos + 2));

                String result = execute_simple_computation(simple_operation);

                if (result == null){
                    return result;
                } else{
                    statement_in_brackets.subList(operand_pos - 1, operand_pos + 2).clear();
                    statement_in_brackets.add(operand_pos - 1, result);

                    higher_priority_operations = (statement_in_brackets.contains("*")) || (statement_in_brackets.contains("/"));
                }
            }
        }

        boolean lower_priority_operations = (statement_in_brackets.contains("+")) || (statement_in_brackets.contains("-"));

        while (lower_priority_operations){

            ArrayList<String> symbols_to_find = new ArrayList<>();
            symbols_to_find.add("+");
            symbols_to_find.add("-");

            String operation = find_operation_symbol(statement_in_brackets, symbols_to_find);

            if (!operation.equals("")){
                int operand_pos = statement_in_brackets.indexOf(operation);
                ArrayList<String> simple_operation = new ArrayList<>(statement_in_brackets.subList(operand_pos - 1, operand_pos + 2));

                String result = execute_simple_computation(simple_operation);

                if (result == null){
                    return result;
                } else{
                    statement_in_brackets.subList(operand_pos - 1, operand_pos + 2).clear();
                    statement_in_brackets.add(operand_pos - 1, result);

                    lower_priority_operations = (statement_in_brackets.contains("-")) || (statement_in_brackets.contains("+"));
                }
            }
        }

        if (statement_in_brackets.size() > 1){
            try {
                return execute_simple_computation(statement_in_brackets);
            } catch (Exception e){
                return null;
            }
        } else {
            return statement_in_brackets.get(0);
        }

    }

    static String find_operation_symbol(ArrayList<String> statement_in_brackets, ArrayList<String> symbols_to_find) {

        for (String symbol: statement_in_brackets) {
            try{
                Double.valueOf(symbol);
            } catch (NumberFormatException e){
                if (symbols_to_find.contains(symbol)){
                    return symbol;
                }
            }

        }

        return "";

    }

    static String execute_simple_computation(ArrayList<String> simple_operation) {

        try{
            switch (simple_operation.get(1)) {
                case "+":
                    return String.valueOf(Double.valueOf(simple_operation.get(0)) + Double.valueOf(simple_operation.get(2)));
                case "-":
                    return String.valueOf(Double.valueOf(simple_operation.get(0)) - Double.valueOf(simple_operation.get(2)));
                case "*":
                    return String.valueOf(Double.valueOf(simple_operation.get(0)) * Double.valueOf(simple_operation.get(2)));
                case "/":
                    if ((simple_operation.get(2).equals("0.0") || (simple_operation.get(2).equals("0")))) {
                        return null;
                    } else {
                        return String.valueOf(Double.valueOf(simple_operation.get(0)) / Double.valueOf(simple_operation.get(2)));
                    }
            }
        } catch (Exception e){
            return null;
        }

        return null;
    }


    static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
  }
